# Development

Run locally:

```sh
$ docker-compose up --build
```

or silently:

```sh
$ docker-compose up -d --build
```

Verify [http://localhost:8000/](http://localhost:8000/) works as expected:

```json
{
  "hello": "world"
}
```

If docker is not available, create a local virtual environment

```sh
$ cd app
$ python3 -m venv env
$ source env/bin/activate
```

```sh
(env) $ python manage.py runserver
```

Setting up a separate database in a dev environment:

```sql
CREATE USER fund4me_dev WITH PASSWORD 'PASSword';
GRANT fund4me_dev TO postgres;
CREATE DATABASE fund4me_dev OWNER = fund4me_dev;
```



# Configuration Guides

Help and explanations for initial configuration.

## GitLab Environment Variables

**PRIVATE_KEY**: RSA key (id_rsa) on EC2 instance.

Obtaining the value:

```sh
[ec2-user@ip- ~]$ sudo cat .ssh/id_rsa
```

**SECRET_KEY**: django's SECRET_KEY



# Resources

 - [Dockerizing Django with Postgres, Gunicorn, and Nginx](https://testdriven.io/blog/dockerizing-django-with-postgres-gunicorn-and-nginx/)
 - [Continuously Deploying Django to AWS EC2 with Docker and GitLab](https://testdriven.io/blog/deploying-django-to-ec2-with-docker-and-gitlab/)
